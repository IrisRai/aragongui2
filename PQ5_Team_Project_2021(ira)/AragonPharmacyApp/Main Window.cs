﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AragonPharmacyApp
{
    public partial class Main_Window : Form
    {
        public Main_Window()
        {
            InitializeComponent();
        }

        private void btnCustomerEntryForm_Click(object sender, EventArgs e)
        {
            Customer dlg = new Customer();
            dlg.Owner = this;
            dlg.ShowDialog();
        }

        private void btnDrugEntryForm_Click(object sender, EventArgs e)
        {
            Drug dlg = new Drug();
            dlg.Owner = this;
            dlg.ShowDialog();
        }

        private void btnDoctorEntryForm_Click(object sender, EventArgs e)
        {
            Doctors dlg = new Doctors();
            dlg.Owner = this;
            dlg.ShowDialog();
        }

        private void btnClinic_Click(object sender, EventArgs e)
        {
            Clinic dlg = new Clinic();
            dlg.Owner = this;
            dlg.ShowDialog();
        }

        private void btnHouseHold_Click(object sender, EventArgs e)
        {
            Household dlg = new Household();
            dlg.Owner = this;
            dlg.ShowDialog();
        }


        private void btnEmployees_Click(object sender, EventArgs e)
        {
            Employees dlg = new Employees();
            dlg.Owner = this;
            dlg.ShowDialog();
        }

        private void btnHealthPlan_Click(object sender, EventArgs e)
        {
            HealthPlan dlg = new HealthPlan();
            dlg.Owner = this;
            dlg.ShowDialog();
        }

        private void btnExitMain_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

       
    }
}
