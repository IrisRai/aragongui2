﻿
namespace AragonPharmacyApp
{
    partial class Clinic
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label clinicIDLabel;
            System.Windows.Forms.Label clinicNameLabel;
            System.Windows.Forms.Label address1Label;
            System.Windows.Forms.Label address2Label;
            System.Windows.Forms.Label cityLabel;
            System.Windows.Forms.Label provinceLabel;
            System.Windows.Forms.Label postalCodeLabel;
            System.Windows.Forms.Label phoneLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Clinic));
            this.aragonPharmacy_2DataSet11 = new AragonPharmacyApp.AragonPharmacy_2DataSet1();
            this.clinicBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.clinicTableAdapter1 = new AragonPharmacyApp.AragonPharmacy_2DataSet1TableAdapters.ClinicTableAdapter();
            this.tableAdapterManager1 = new AragonPharmacyApp.AragonPharmacy_2DataSet1TableAdapters.TableAdapterManager();
            this.clinicBindingSource1BindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.clinicBindingSource1BindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.clinicIDTextBox1 = new System.Windows.Forms.TextBox();
            this.clinicNameTextBox1 = new System.Windows.Forms.TextBox();
            this.address1TextBox1 = new System.Windows.Forms.TextBox();
            this.address2TextBox1 = new System.Windows.Forms.TextBox();
            this.cityTextBox1 = new System.Windows.Forms.TextBox();
            this.provinceTextBox1 = new System.Windows.Forms.TextBox();
            this.postalCodeTextBox = new System.Windows.Forms.TextBox();
            this.phoneTextBox = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnReturnClinic = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblClinic = new System.Windows.Forms.Label();
            clinicIDLabel = new System.Windows.Forms.Label();
            clinicNameLabel = new System.Windows.Forms.Label();
            address1Label = new System.Windows.Forms.Label();
            address2Label = new System.Windows.Forms.Label();
            cityLabel = new System.Windows.Forms.Label();
            provinceLabel = new System.Windows.Forms.Label();
            postalCodeLabel = new System.Windows.Forms.Label();
            phoneLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.aragonPharmacy_2DataSet11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clinicBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clinicBindingSource1BindingNavigator)).BeginInit();
            this.clinicBindingSource1BindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // clinicIDLabel
            // 
            clinicIDLabel.AutoSize = true;
            clinicIDLabel.Location = new System.Drawing.Point(12, 147);
            clinicIDLabel.Name = "clinicIDLabel";
            clinicIDLabel.Size = new System.Drawing.Size(49, 13);
            clinicIDLabel.TabIndex = 1;
            clinicIDLabel.Text = "Clinic ID:";
            // 
            // clinicNameLabel
            // 
            clinicNameLabel.AutoSize = true;
            clinicNameLabel.Location = new System.Drawing.Point(12, 172);
            clinicNameLabel.Name = "clinicNameLabel";
            clinicNameLabel.Size = new System.Drawing.Size(66, 13);
            clinicNameLabel.TabIndex = 3;
            clinicNameLabel.Text = "Clinic Name:";
            // 
            // address1Label
            // 
            address1Label.AutoSize = true;
            address1Label.Location = new System.Drawing.Point(12, 197);
            address1Label.Name = "address1Label";
            address1Label.Size = new System.Drawing.Size(54, 13);
            address1Label.TabIndex = 5;
            address1Label.Text = "Address1:";
            // 
            // address2Label
            // 
            address2Label.AutoSize = true;
            address2Label.Location = new System.Drawing.Point(12, 223);
            address2Label.Name = "address2Label";
            address2Label.Size = new System.Drawing.Size(54, 13);
            address2Label.TabIndex = 7;
            address2Label.Text = "Address2:";
            // 
            // cityLabel
            // 
            cityLabel.AutoSize = true;
            cityLabel.Location = new System.Drawing.Point(12, 249);
            cityLabel.Name = "cityLabel";
            cityLabel.Size = new System.Drawing.Size(27, 13);
            cityLabel.TabIndex = 9;
            cityLabel.Text = "City:";
            // 
            // provinceLabel
            // 
            provinceLabel.AutoSize = true;
            provinceLabel.Location = new System.Drawing.Point(12, 275);
            provinceLabel.Name = "provinceLabel";
            provinceLabel.Size = new System.Drawing.Size(52, 13);
            provinceLabel.TabIndex = 11;
            provinceLabel.Text = "Province:";
            // 
            // postalCodeLabel
            // 
            postalCodeLabel.AutoSize = true;
            postalCodeLabel.Location = new System.Drawing.Point(12, 301);
            postalCodeLabel.Name = "postalCodeLabel";
            postalCodeLabel.Size = new System.Drawing.Size(67, 13);
            postalCodeLabel.TabIndex = 13;
            postalCodeLabel.Text = "Postal Code:";
            // 
            // phoneLabel
            // 
            phoneLabel.AutoSize = true;
            phoneLabel.Location = new System.Drawing.Point(12, 327);
            phoneLabel.Name = "phoneLabel";
            phoneLabel.Size = new System.Drawing.Size(41, 13);
            phoneLabel.TabIndex = 15;
            phoneLabel.Text = "Phone:";
            // 
            // aragonPharmacy_2DataSet11
            // 
            this.aragonPharmacy_2DataSet11.DataSetName = "AragonPharmacy_2DataSet1";
            this.aragonPharmacy_2DataSet11.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // clinicBindingSource1
            // 
            this.clinicBindingSource1.DataMember = "Clinic";
            this.clinicBindingSource1.DataSource = this.aragonPharmacy_2DataSet11;
            // 
            // clinicTableAdapter1
            // 
            this.clinicTableAdapter1.ClearBeforeFill = true;
            // 
            // tableAdapterManager1
            // 
            this.tableAdapterManager1.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager1.ClinicTableAdapter = this.clinicTableAdapter1;
            this.tableAdapterManager1.UpdateOrder = AragonPharmacyApp.AragonPharmacy_2DataSet1TableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // clinicBindingSource1BindingNavigator
            // 
            this.clinicBindingSource1BindingNavigator.AddNewItem = this.toolStripButton5;
            this.clinicBindingSource1BindingNavigator.BackColor = System.Drawing.SystemColors.Control;
            this.clinicBindingSource1BindingNavigator.BindingSource = this.clinicBindingSource1;
            this.clinicBindingSource1BindingNavigator.CountItem = this.toolStripLabel1;
            this.clinicBindingSource1BindingNavigator.DeleteItem = this.toolStripButton6;
            this.clinicBindingSource1BindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton2,
            this.toolStripSeparator1,
            this.toolStripTextBox1,
            this.toolStripLabel1,
            this.toolStripSeparator2,
            this.toolStripButton3,
            this.toolStripButton4,
            this.toolStripSeparator3,
            this.toolStripButton5,
            this.toolStripButton6,
            this.clinicBindingSource1BindingNavigatorSaveItem});
            this.clinicBindingSource1BindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.clinicBindingSource1BindingNavigator.MoveFirstItem = this.toolStripButton1;
            this.clinicBindingSource1BindingNavigator.MoveLastItem = this.toolStripButton4;
            this.clinicBindingSource1BindingNavigator.MoveNextItem = this.toolStripButton3;
            this.clinicBindingSource1BindingNavigator.MovePreviousItem = this.toolStripButton2;
            this.clinicBindingSource1BindingNavigator.Name = "clinicBindingSource1BindingNavigator";
            this.clinicBindingSource1BindingNavigator.PositionItem = this.toolStripTextBox1;
            this.clinicBindingSource1BindingNavigator.Size = new System.Drawing.Size(401, 25);
            this.clinicBindingSource1BindingNavigator.TabIndex = 0;
            this.clinicBindingSource1BindingNavigator.Text = "bindingNavigator1";
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton5.Image")));
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.RightToLeftAutoMirrorImage = true;
            this.toolStripButton5.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton5.Text = "Add new";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(35, 22);
            this.toolStripLabel1.Text = "of {0}";
            this.toolStripLabel1.ToolTipText = "Total number of items";
            // 
            // toolStripButton6
            // 
            this.toolStripButton6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton6.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton6.Image")));
            this.toolStripButton6.Name = "toolStripButton6";
            this.toolStripButton6.RightToLeftAutoMirrorImage = true;
            this.toolStripButton6.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton6.Text = "Delete";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.RightToLeftAutoMirrorImage = true;
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "Move first";
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.RightToLeftAutoMirrorImage = true;
            this.toolStripButton2.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton2.Text = "Move previous";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.AccessibleName = "Position";
            this.toolStripTextBox1.AutoSize = false;
            this.toolStripTextBox1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(50, 23);
            this.toolStripTextBox1.Text = "0";
            this.toolStripTextBox1.ToolTipText = "Current position";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.RightToLeftAutoMirrorImage = true;
            this.toolStripButton3.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton3.Text = "Move next";
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.RightToLeftAutoMirrorImage = true;
            this.toolStripButton4.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton4.Text = "Move last";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // clinicBindingSource1BindingNavigatorSaveItem
            // 
            this.clinicBindingSource1BindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.clinicBindingSource1BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("clinicBindingSource1BindingNavigatorSaveItem.Image")));
            this.clinicBindingSource1BindingNavigatorSaveItem.Name = "clinicBindingSource1BindingNavigatorSaveItem";
            this.clinicBindingSource1BindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.clinicBindingSource1BindingNavigatorSaveItem.Text = "Save Data";
            this.clinicBindingSource1BindingNavigatorSaveItem.Click += new System.EventHandler(this.clinicBindingSource1BindingNavigatorSaveItem_Click);
            // 
            // clinicIDTextBox1
            // 
            this.clinicIDTextBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clinicBindingSource1, "ClinicID", true));
            this.clinicIDTextBox1.Location = new System.Drawing.Point(86, 144);
            this.clinicIDTextBox1.Name = "clinicIDTextBox1";
            this.clinicIDTextBox1.Size = new System.Drawing.Size(100, 20);
            this.clinicIDTextBox1.TabIndex = 2;
            // 
            // clinicNameTextBox1
            // 
            this.clinicNameTextBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clinicBindingSource1, "ClinicName", true));
            this.clinicNameTextBox1.Location = new System.Drawing.Point(86, 169);
            this.clinicNameTextBox1.Name = "clinicNameTextBox1";
            this.clinicNameTextBox1.Size = new System.Drawing.Size(200, 20);
            this.clinicNameTextBox1.TabIndex = 4;
            // 
            // address1TextBox1
            // 
            this.address1TextBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clinicBindingSource1, "Address1", true));
            this.address1TextBox1.Location = new System.Drawing.Point(86, 194);
            this.address1TextBox1.Name = "address1TextBox1";
            this.address1TextBox1.Size = new System.Drawing.Size(200, 20);
            this.address1TextBox1.TabIndex = 6;
            // 
            // address2TextBox1
            // 
            this.address2TextBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clinicBindingSource1, "Address2", true));
            this.address2TextBox1.Location = new System.Drawing.Point(86, 220);
            this.address2TextBox1.Name = "address2TextBox1";
            this.address2TextBox1.Size = new System.Drawing.Size(200, 20);
            this.address2TextBox1.TabIndex = 8;
            // 
            // cityTextBox1
            // 
            this.cityTextBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clinicBindingSource1, "City", true));
            this.cityTextBox1.Location = new System.Drawing.Point(86, 246);
            this.cityTextBox1.Name = "cityTextBox1";
            this.cityTextBox1.Size = new System.Drawing.Size(200, 20);
            this.cityTextBox1.TabIndex = 10;
            // 
            // provinceTextBox1
            // 
            this.provinceTextBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clinicBindingSource1, "PostalCode", true));
            this.provinceTextBox1.Location = new System.Drawing.Point(86, 272);
            this.provinceTextBox1.Name = "provinceTextBox1";
            this.provinceTextBox1.Size = new System.Drawing.Size(100, 20);
            this.provinceTextBox1.TabIndex = 12;
            // 
            // postalCodeTextBox
            // 
            this.postalCodeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clinicBindingSource1, "PostalCode", true));
            this.postalCodeTextBox.Location = new System.Drawing.Point(86, 298);
            this.postalCodeTextBox.Name = "postalCodeTextBox";
            this.postalCodeTextBox.Size = new System.Drawing.Size(100, 20);
            this.postalCodeTextBox.TabIndex = 14;
            // 
            // phoneTextBox
            // 
            this.phoneTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.clinicBindingSource1, "Phone", true));
            this.phoneTextBox.Location = new System.Drawing.Point(87, 325);
            this.phoneTextBox.Name = "phoneTextBox";
            this.phoneTextBox.Size = new System.Drawing.Size(100, 20);
            this.phoneTextBox.TabIndex = 16;
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.Color.Brown;
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.btnSearch.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnSearch.Location = new System.Drawing.Point(61, 374);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(125, 30);
            this.btnSearch.TabIndex = 61;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = false;
            // 
            // btnReturnClinic
            // 
            this.btnReturnClinic.BackColor = System.Drawing.Color.Brown;
            this.btnReturnClinic.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.btnReturnClinic.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnReturnClinic.Location = new System.Drawing.Point(214, 374);
            this.btnReturnClinic.Name = "btnReturnClinic";
            this.btnReturnClinic.Size = new System.Drawing.Size(125, 30);
            this.btnReturnClinic.TabIndex = 60;
            this.btnReturnClinic.Text = "Return";
            this.btnReturnClinic.UseVisualStyleBackColor = false;
            this.btnReturnClinic.Click += new System.EventHandler(this.btnReturnClinic_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 28);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(106, 89);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 68;
            this.pictureBox1.TabStop = false;
            // 
            // lblClinic
            // 
            this.lblClinic.AutoSize = true;
            this.lblClinic.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClinic.ForeColor = System.Drawing.Color.MidnightBlue;
            this.lblClinic.Location = new System.Drawing.Point(124, 52);
            this.lblClinic.Name = "lblClinic";
            this.lblClinic.Size = new System.Drawing.Size(101, 37);
            this.lblClinic.TabIndex = 67;
            this.lblClinic.Text = "Clinic";
            // 
            // Clinic
            // 
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(401, 416);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblClinic);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.btnReturnClinic);
            this.Controls.Add(phoneLabel);
            this.Controls.Add(this.phoneTextBox);
            this.Controls.Add(postalCodeLabel);
            this.Controls.Add(this.postalCodeTextBox);
            this.Controls.Add(provinceLabel);
            this.Controls.Add(this.provinceTextBox1);
            this.Controls.Add(cityLabel);
            this.Controls.Add(this.cityTextBox1);
            this.Controls.Add(address2Label);
            this.Controls.Add(this.address2TextBox1);
            this.Controls.Add(address1Label);
            this.Controls.Add(this.address1TextBox1);
            this.Controls.Add(clinicNameLabel);
            this.Controls.Add(this.clinicNameTextBox1);
            this.Controls.Add(clinicIDLabel);
            this.Controls.Add(this.clinicIDTextBox1);
            this.Controls.Add(this.clinicBindingSource1BindingNavigator);
            this.Name = "Clinic";
            this.Load += new System.EventHandler(this.Clinic_Load_1);
            ((System.ComponentModel.ISupportInitialize)(this.aragonPharmacy_2DataSet11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clinicBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clinicBindingSource1BindingNavigator)).EndInit();
            this.clinicBindingSource1BindingNavigator.ResumeLayout(false);
            this.clinicBindingSource1BindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private AragonPharmacy_2DataSet1 aragonPharmacy_2DataSet1;
        private System.Windows.Forms.BindingSource clinicBindingSource;
        private AragonPharmacy_2DataSet1TableAdapters.ClinicTableAdapter clinicTableAdapter;
        private AragonPharmacy_2DataSet1TableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator clinicBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton clinicBindingNavigatorSaveItem;
        private System.Windows.Forms.TextBox clinicIDTextBox;
        private System.Windows.Forms.TextBox clinicNameTextBox;
        private System.Windows.Forms.TextBox address1TextBox;
        private System.Windows.Forms.TextBox address2TextBox;
        private System.Windows.Forms.TextBox cityTextBox;
        private System.Windows.Forms.TextBox provinceTextBox;
        private System.Windows.Forms.TextBox txtPostalCode;
        private System.Windows.Forms.TextBox txtPhone;
        private AragonPharmacy_2DataSet1 aragonPharmacy_2DataSet11;
        private System.Windows.Forms.BindingSource clinicBindingSource1;
        private AragonPharmacy_2DataSet1TableAdapters.ClinicTableAdapter clinicTableAdapter1;
        private AragonPharmacy_2DataSet1TableAdapters.TableAdapterManager tableAdapterManager1;
        private System.Windows.Forms.BindingNavigator clinicBindingSource1BindingNavigator;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripButton toolStripButton6;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton clinicBindingSource1BindingNavigatorSaveItem;
        private System.Windows.Forms.TextBox clinicIDTextBox1;
        private System.Windows.Forms.TextBox clinicNameTextBox1;
        private System.Windows.Forms.TextBox address1TextBox1;
        private System.Windows.Forms.TextBox address2TextBox1;
        private System.Windows.Forms.TextBox cityTextBox1;
        private System.Windows.Forms.TextBox provinceTextBox1;
        private System.Windows.Forms.TextBox postalCodeTextBox;
        private System.Windows.Forms.TextBox phoneTextBox;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnReturnClinic;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblClinic;
    }
}