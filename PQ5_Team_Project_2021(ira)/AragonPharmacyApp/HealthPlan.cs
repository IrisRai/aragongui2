﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AragonPharmacyApp
{
    public partial class HealthPlan: Form
    {
        public HealthPlan()
        {
            InitializeComponent();
        }

        private void healthPlanBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.healthPlanBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.aragonPharmacy_2DataSet3);

        }

        private void HealthPlan_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'aragonPharmacy_2DataSet3.HealthPlan' table. You can move, or remove it, as needed.
            this.healthPlanTableAdapter.Fill(this.aragonPharmacy_2DataSet3.HealthPlan);

        }


        private void btnReturn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
