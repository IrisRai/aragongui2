﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AragonPharmacyApp
{
    public partial class Doctors : Form
    {
        public Doctors()
        {
            InitializeComponent();
        }


        private void doctorBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.doctorBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.aragonPharmacy_2DataSet6);

        }


        private void Doctors_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'aragonPharmacy_2DataSet6.Doctor' table. You can move, or remove it, as needed.
            this.doctorTableAdapter.Fill(this.aragonPharmacy_2DataSet6.Doctor);

        }

        private void btnReturnDoc_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            string ConString = @"Data Source=localhost;Initial Catalog=AragonPharmacy_2;Integrated Security=True";
            string Query = "SELECT * FROM InformationSystem.Doctor";

            SqlDataAdapter adapter = new SqlDataAdapter(Query, ConString);
            DataSet set = new DataSet();

            adapter.Fill(set, "InformationSystem.Doctor");
            dataGridView1.DataSource = set.Tables["InformationSystem.Doctor"];
        }
    }
}

