﻿namespace AragonPharmacyApp
{
    partial class HealthPlan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label planIDLabel;
            System.Windows.Forms.Label planNameLabel;
            System.Windows.Forms.Label addressLabel;
            System.Windows.Forms.Label cityLabel;
            System.Windows.Forms.Label provinceLabel;
            System.Windows.Forms.Label postalCodeLabel;
            System.Windows.Forms.Label phoneLabel;
            System.Windows.Forms.Label daysLabel;
            System.Windows.Forms.Label webSiteLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HealthPlan));
            this.aragonPharmacy_2DataSet3 = new AragonPharmacyApp.AragonPharmacy_2DataSet3();
            this.healthPlanBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.healthPlanTableAdapter = new AragonPharmacyApp.AragonPharmacy_2DataSet3TableAdapters.HealthPlanTableAdapter();
            this.tableAdapterManager = new AragonPharmacyApp.AragonPharmacy_2DataSet3TableAdapters.TableAdapterManager();
            this.healthPlanBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.healthPlanBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.planIDTextBox = new System.Windows.Forms.TextBox();
            this.planNameTextBox = new System.Windows.Forms.TextBox();
            this.addressTextBox = new System.Windows.Forms.TextBox();
            this.cityTextBox = new System.Windows.Forms.TextBox();
            this.provinceTextBox = new System.Windows.Forms.TextBox();
            this.postalCodeTextBox = new System.Windows.Forms.TextBox();
            this.phoneTextBox = new System.Windows.Forms.TextBox();
            this.daysTextBox = new System.Windows.Forms.TextBox();
            this.webSiteTextBox = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnReturn = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblHealthPlan = new System.Windows.Forms.Label();
            planIDLabel = new System.Windows.Forms.Label();
            planNameLabel = new System.Windows.Forms.Label();
            addressLabel = new System.Windows.Forms.Label();
            cityLabel = new System.Windows.Forms.Label();
            provinceLabel = new System.Windows.Forms.Label();
            postalCodeLabel = new System.Windows.Forms.Label();
            phoneLabel = new System.Windows.Forms.Label();
            daysLabel = new System.Windows.Forms.Label();
            webSiteLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.aragonPharmacy_2DataSet3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.healthPlanBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.healthPlanBindingNavigator)).BeginInit();
            this.healthPlanBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // planIDLabel
            // 
            planIDLabel.AutoSize = true;
            planIDLabel.Location = new System.Drawing.Point(21, 155);
            planIDLabel.Name = "planIDLabel";
            planIDLabel.Size = new System.Drawing.Size(45, 13);
            planIDLabel.TabIndex = 1;
            planIDLabel.Text = "Plan ID:";
            // 
            // planNameLabel
            // 
            planNameLabel.AutoSize = true;
            planNameLabel.Location = new System.Drawing.Point(21, 181);
            planNameLabel.Name = "planNameLabel";
            planNameLabel.Size = new System.Drawing.Size(62, 13);
            planNameLabel.TabIndex = 3;
            planNameLabel.Text = "Plan Name:";
            // 
            // addressLabel
            // 
            addressLabel.AutoSize = true;
            addressLabel.Location = new System.Drawing.Point(21, 207);
            addressLabel.Name = "addressLabel";
            addressLabel.Size = new System.Drawing.Size(48, 13);
            addressLabel.TabIndex = 5;
            addressLabel.Text = "Address:";
            // 
            // cityLabel
            // 
            cityLabel.AutoSize = true;
            cityLabel.Location = new System.Drawing.Point(21, 233);
            cityLabel.Name = "cityLabel";
            cityLabel.Size = new System.Drawing.Size(27, 13);
            cityLabel.TabIndex = 7;
            cityLabel.Text = "City:";
            // 
            // provinceLabel
            // 
            provinceLabel.AutoSize = true;
            provinceLabel.Location = new System.Drawing.Point(220, 155);
            provinceLabel.Name = "provinceLabel";
            provinceLabel.Size = new System.Drawing.Size(52, 13);
            provinceLabel.TabIndex = 9;
            provinceLabel.Text = "Province:";
            // 
            // postalCodeLabel
            // 
            postalCodeLabel.AutoSize = true;
            postalCodeLabel.Location = new System.Drawing.Point(220, 181);
            postalCodeLabel.Name = "postalCodeLabel";
            postalCodeLabel.Size = new System.Drawing.Size(67, 13);
            postalCodeLabel.TabIndex = 11;
            postalCodeLabel.Text = "Postal Code:";
            // 
            // phoneLabel
            // 
            phoneLabel.AutoSize = true;
            phoneLabel.Location = new System.Drawing.Point(220, 207);
            phoneLabel.Name = "phoneLabel";
            phoneLabel.Size = new System.Drawing.Size(41, 13);
            phoneLabel.TabIndex = 13;
            phoneLabel.Text = "Phone:";
            // 
            // daysLabel
            // 
            daysLabel.AutoSize = true;
            daysLabel.Location = new System.Drawing.Point(220, 233);
            daysLabel.Name = "daysLabel";
            daysLabel.Size = new System.Drawing.Size(34, 13);
            daysLabel.TabIndex = 15;
            daysLabel.Text = "Days:";
            // 
            // webSiteLabel
            // 
            webSiteLabel.AutoSize = true;
            webSiteLabel.Location = new System.Drawing.Point(86, 285);
            webSiteLabel.Name = "webSiteLabel";
            webSiteLabel.Size = new System.Drawing.Size(54, 13);
            webSiteLabel.TabIndex = 17;
            webSiteLabel.Text = "Web Site:";
            // 
            // aragonPharmacy_2DataSet3
            // 
            this.aragonPharmacy_2DataSet3.DataSetName = "AragonPharmacy_2DataSet3";
            this.aragonPharmacy_2DataSet3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // healthPlanBindingSource
            // 
            this.healthPlanBindingSource.DataMember = "HealthPlan";
            this.healthPlanBindingSource.DataSource = this.aragonPharmacy_2DataSet3;
            // 
            // healthPlanTableAdapter
            // 
            this.healthPlanTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.HealthPlanTableAdapter = this.healthPlanTableAdapter;
            this.tableAdapterManager.UpdateOrder = AragonPharmacyApp.AragonPharmacy_2DataSet3TableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // healthPlanBindingNavigator
            // 
            this.healthPlanBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.healthPlanBindingNavigator.BindingSource = this.healthPlanBindingSource;
            this.healthPlanBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.healthPlanBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.healthPlanBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.healthPlanBindingNavigatorSaveItem});
            this.healthPlanBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.healthPlanBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.healthPlanBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.healthPlanBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.healthPlanBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.healthPlanBindingNavigator.Name = "healthPlanBindingNavigator";
            this.healthPlanBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.healthPlanBindingNavigator.Size = new System.Drawing.Size(421, 25);
            this.healthPlanBindingNavigator.TabIndex = 0;
            this.healthPlanBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // healthPlanBindingNavigatorSaveItem
            // 
            this.healthPlanBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.healthPlanBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("healthPlanBindingNavigatorSaveItem.Image")));
            this.healthPlanBindingNavigatorSaveItem.Name = "healthPlanBindingNavigatorSaveItem";
            this.healthPlanBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.healthPlanBindingNavigatorSaveItem.Text = "Save Data";
            this.healthPlanBindingNavigatorSaveItem.Click += new System.EventHandler(this.healthPlanBindingNavigatorSaveItem_Click);
            // 
            // planIDTextBox
            // 
            this.planIDTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.healthPlanBindingSource, "PlanID", true));
            this.planIDTextBox.Location = new System.Drawing.Point(89, 152);
            this.planIDTextBox.Name = "planIDTextBox";
            this.planIDTextBox.Size = new System.Drawing.Size(100, 20);
            this.planIDTextBox.TabIndex = 2;
            // 
            // planNameTextBox
            // 
            this.planNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.healthPlanBindingSource, "PlanName", true));
            this.planNameTextBox.Location = new System.Drawing.Point(89, 178);
            this.planNameTextBox.Name = "planNameTextBox";
            this.planNameTextBox.Size = new System.Drawing.Size(100, 20);
            this.planNameTextBox.TabIndex = 4;
            // 
            // addressTextBox
            // 
            this.addressTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.healthPlanBindingSource, "Address", true));
            this.addressTextBox.Location = new System.Drawing.Point(89, 204);
            this.addressTextBox.Name = "addressTextBox";
            this.addressTextBox.Size = new System.Drawing.Size(100, 20);
            this.addressTextBox.TabIndex = 6;
            // 
            // cityTextBox
            // 
            this.cityTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.healthPlanBindingSource, "City", true));
            this.cityTextBox.Location = new System.Drawing.Point(89, 230);
            this.cityTextBox.Name = "cityTextBox";
            this.cityTextBox.Size = new System.Drawing.Size(100, 20);
            this.cityTextBox.TabIndex = 8;
            // 
            // provinceTextBox
            // 
            this.provinceTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.healthPlanBindingSource, "Province", true));
            this.provinceTextBox.Location = new System.Drawing.Point(293, 152);
            this.provinceTextBox.Name = "provinceTextBox";
            this.provinceTextBox.Size = new System.Drawing.Size(100, 20);
            this.provinceTextBox.TabIndex = 10;
            // 
            // postalCodeTextBox
            // 
            this.postalCodeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.healthPlanBindingSource, "PostalCode", true));
            this.postalCodeTextBox.Location = new System.Drawing.Point(293, 178);
            this.postalCodeTextBox.Name = "postalCodeTextBox";
            this.postalCodeTextBox.Size = new System.Drawing.Size(100, 20);
            this.postalCodeTextBox.TabIndex = 12;
            // 
            // phoneTextBox
            // 
            this.phoneTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.healthPlanBindingSource, "Phone", true));
            this.phoneTextBox.Location = new System.Drawing.Point(293, 204);
            this.phoneTextBox.Name = "phoneTextBox";
            this.phoneTextBox.Size = new System.Drawing.Size(100, 20);
            this.phoneTextBox.TabIndex = 14;
            // 
            // daysTextBox
            // 
            this.daysTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.healthPlanBindingSource, "Days", true));
            this.daysTextBox.Location = new System.Drawing.Point(293, 230);
            this.daysTextBox.Name = "daysTextBox";
            this.daysTextBox.Size = new System.Drawing.Size(100, 20);
            this.daysTextBox.TabIndex = 16;
            // 
            // webSiteTextBox
            // 
            this.webSiteTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.healthPlanBindingSource, "WebSite", true));
            this.webSiteTextBox.Location = new System.Drawing.Point(146, 282);
            this.webSiteTextBox.Name = "webSiteTextBox";
            this.webSiteTextBox.Size = new System.Drawing.Size(170, 20);
            this.webSiteTextBox.TabIndex = 18;
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.Color.Brown;
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.btnSearch.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnSearch.Location = new System.Drawing.Point(64, 347);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(125, 30);
            this.btnSearch.TabIndex = 61;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = false;
            // 
            // btnReturn
            // 
            this.btnReturn.BackColor = System.Drawing.Color.Brown;
            this.btnReturn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.btnReturn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnReturn.Location = new System.Drawing.Point(236, 347);
            this.btnReturn.Name = "btnReturn";
            this.btnReturn.Size = new System.Drawing.Size(125, 30);
            this.btnReturn.TabIndex = 60;
            this.btnReturn.Text = "Return";
            this.btnReturn.UseVisualStyleBackColor = false;
            this.btnReturn.Click += new System.EventHandler(this.btnReturn_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 28);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(106, 89);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 68;
            this.pictureBox1.TabStop = false;
            // 
            // lblHealthPlan
            // 
            this.lblHealthPlan.AutoSize = true;
            this.lblHealthPlan.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHealthPlan.ForeColor = System.Drawing.Color.MidnightBlue;
            this.lblHealthPlan.Location = new System.Drawing.Point(124, 54);
            this.lblHealthPlan.Name = "lblHealthPlan";
            this.lblHealthPlan.Size = new System.Drawing.Size(183, 37);
            this.lblHealthPlan.TabIndex = 67;
            this.lblHealthPlan.Text = "HealthPlan";
            // 
            // HealthPlan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(421, 408);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblHealthPlan);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.btnReturn);
            this.Controls.Add(webSiteLabel);
            this.Controls.Add(this.webSiteTextBox);
            this.Controls.Add(daysLabel);
            this.Controls.Add(this.daysTextBox);
            this.Controls.Add(phoneLabel);
            this.Controls.Add(this.phoneTextBox);
            this.Controls.Add(postalCodeLabel);
            this.Controls.Add(this.postalCodeTextBox);
            this.Controls.Add(provinceLabel);
            this.Controls.Add(this.provinceTextBox);
            this.Controls.Add(cityLabel);
            this.Controls.Add(this.cityTextBox);
            this.Controls.Add(addressLabel);
            this.Controls.Add(this.addressTextBox);
            this.Controls.Add(planNameLabel);
            this.Controls.Add(this.planNameTextBox);
            this.Controls.Add(planIDLabel);
            this.Controls.Add(this.planIDTextBox);
            this.Controls.Add(this.healthPlanBindingNavigator);
            this.Name = "HealthPlan";
            this.Text = "HealthPlan";
            this.Load += new System.EventHandler(this.HealthPlan_Load);
            ((System.ComponentModel.ISupportInitialize)(this.aragonPharmacy_2DataSet3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.healthPlanBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.healthPlanBindingNavigator)).EndInit();
            this.healthPlanBindingNavigator.ResumeLayout(false);
            this.healthPlanBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private AragonPharmacy_2DataSet3 aragonPharmacy_2DataSet3;
        private System.Windows.Forms.BindingSource healthPlanBindingSource;
        private AragonPharmacy_2DataSet3TableAdapters.HealthPlanTableAdapter healthPlanTableAdapter;
        private AragonPharmacy_2DataSet3TableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator healthPlanBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton healthPlanBindingNavigatorSaveItem;
        private System.Windows.Forms.TextBox planIDTextBox;
        private System.Windows.Forms.TextBox planNameTextBox;
        private System.Windows.Forms.TextBox addressTextBox;
        private System.Windows.Forms.TextBox cityTextBox;
        private System.Windows.Forms.TextBox provinceTextBox;
        private System.Windows.Forms.TextBox postalCodeTextBox;
        private System.Windows.Forms.TextBox phoneTextBox;
        private System.Windows.Forms.TextBox daysTextBox;
        private System.Windows.Forms.TextBox webSiteTextBox;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnReturn;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblHealthPlan;
    }
}