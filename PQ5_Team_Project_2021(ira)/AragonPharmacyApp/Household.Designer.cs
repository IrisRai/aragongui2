﻿
namespace AragonPharmacyApp
{
    partial class Household
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label houseIDLabel;
            System.Windows.Forms.Label addressLabel;
            System.Windows.Forms.Label cityLabel;
            System.Windows.Forms.Label provinceLabel;
            System.Windows.Forms.Label postalCodeLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Household));
            this.aragonPharmacy_2DataSet2 = new AragonPharmacyApp.AragonPharmacy_2DataSet2();
            this.householdBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.householdTableAdapter1 = new AragonPharmacyApp.AragonPharmacy_2DataSet2TableAdapters.HouseholdTableAdapter();
            this.tableAdapterManager1 = new AragonPharmacyApp.AragonPharmacy_2DataSet2TableAdapters.TableAdapterManager();
            this.householdBindingSource1BindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.householdBindingSource1BindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.houseIDTextBox1 = new System.Windows.Forms.TextBox();
            this.addressTextBox1 = new System.Windows.Forms.TextBox();
            this.cityTextBox1 = new System.Windows.Forms.TextBox();
            this.provinceTextBox1 = new System.Windows.Forms.TextBox();
            this.postalCodeTextBox1 = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnReturn = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblHouseHold = new System.Windows.Forms.Label();
            houseIDLabel = new System.Windows.Forms.Label();
            addressLabel = new System.Windows.Forms.Label();
            cityLabel = new System.Windows.Forms.Label();
            provinceLabel = new System.Windows.Forms.Label();
            postalCodeLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.aragonPharmacy_2DataSet2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.householdBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.householdBindingSource1BindingNavigator)).BeginInit();
            this.householdBindingSource1BindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // houseIDLabel
            // 
            houseIDLabel.AutoSize = true;
            houseIDLabel.Location = new System.Drawing.Point(21, 151);
            houseIDLabel.Name = "houseIDLabel";
            houseIDLabel.Size = new System.Drawing.Size(55, 13);
            houseIDLabel.TabIndex = 1;
            houseIDLabel.Text = "House ID:";
            // 
            // addressLabel
            // 
            addressLabel.AutoSize = true;
            addressLabel.Location = new System.Drawing.Point(21, 177);
            addressLabel.Name = "addressLabel";
            addressLabel.Size = new System.Drawing.Size(48, 13);
            addressLabel.TabIndex = 3;
            addressLabel.Text = "Address:";
            // 
            // cityLabel
            // 
            cityLabel.AutoSize = true;
            cityLabel.Location = new System.Drawing.Point(21, 203);
            cityLabel.Name = "cityLabel";
            cityLabel.Size = new System.Drawing.Size(27, 13);
            cityLabel.TabIndex = 5;
            cityLabel.Text = "City:";
            // 
            // provinceLabel
            // 
            provinceLabel.AutoSize = true;
            provinceLabel.Location = new System.Drawing.Point(21, 229);
            provinceLabel.Name = "provinceLabel";
            provinceLabel.Size = new System.Drawing.Size(52, 13);
            provinceLabel.TabIndex = 7;
            provinceLabel.Text = "Province:";
            // 
            // postalCodeLabel
            // 
            postalCodeLabel.AutoSize = true;
            postalCodeLabel.Location = new System.Drawing.Point(21, 255);
            postalCodeLabel.Name = "postalCodeLabel";
            postalCodeLabel.Size = new System.Drawing.Size(67, 13);
            postalCodeLabel.TabIndex = 9;
            postalCodeLabel.Text = "Postal Code:";
            // 
            // aragonPharmacy_2DataSet2
            // 
            this.aragonPharmacy_2DataSet2.DataSetName = "AragonPharmacy_2DataSet2";
            this.aragonPharmacy_2DataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // householdBindingSource1
            // 
            this.householdBindingSource1.DataMember = "Household";
            this.householdBindingSource1.DataSource = this.aragonPharmacy_2DataSet2;
            // 
            // householdTableAdapter1
            // 
            this.householdTableAdapter1.ClearBeforeFill = true;
            // 
            // tableAdapterManager1
            // 
            this.tableAdapterManager1.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager1.HouseholdTableAdapter = this.householdTableAdapter1;
            this.tableAdapterManager1.UpdateOrder = AragonPharmacyApp.AragonPharmacy_2DataSet2TableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // householdBindingSource1BindingNavigator
            // 
            this.householdBindingSource1BindingNavigator.AddNewItem = this.toolStripButton5;
            this.householdBindingSource1BindingNavigator.BindingSource = this.householdBindingSource1;
            this.householdBindingSource1BindingNavigator.CountItem = this.toolStripLabel1;
            this.householdBindingSource1BindingNavigator.DeleteItem = this.toolStripButton6;
            this.householdBindingSource1BindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton2,
            this.toolStripSeparator1,
            this.toolStripTextBox1,
            this.toolStripLabel1,
            this.toolStripSeparator2,
            this.toolStripButton3,
            this.toolStripButton4,
            this.toolStripSeparator3,
            this.toolStripButton5,
            this.toolStripButton6,
            this.householdBindingSource1BindingNavigatorSaveItem});
            this.householdBindingSource1BindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.householdBindingSource1BindingNavigator.MoveFirstItem = this.toolStripButton1;
            this.householdBindingSource1BindingNavigator.MoveLastItem = this.toolStripButton4;
            this.householdBindingSource1BindingNavigator.MoveNextItem = this.toolStripButton3;
            this.householdBindingSource1BindingNavigator.MovePreviousItem = this.toolStripButton2;
            this.householdBindingSource1BindingNavigator.Name = "householdBindingSource1BindingNavigator";
            this.householdBindingSource1BindingNavigator.PositionItem = this.toolStripTextBox1;
            this.householdBindingSource1BindingNavigator.Size = new System.Drawing.Size(359, 25);
            this.householdBindingSource1BindingNavigator.TabIndex = 0;
            this.householdBindingSource1BindingNavigator.Text = "bindingNavigator1";
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton5.Image")));
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.RightToLeftAutoMirrorImage = true;
            this.toolStripButton5.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton5.Text = "Add new";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(35, 22);
            this.toolStripLabel1.Text = "of {0}";
            this.toolStripLabel1.ToolTipText = "Total number of items";
            // 
            // toolStripButton6
            // 
            this.toolStripButton6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton6.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton6.Image")));
            this.toolStripButton6.Name = "toolStripButton6";
            this.toolStripButton6.RightToLeftAutoMirrorImage = true;
            this.toolStripButton6.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton6.Text = "Delete";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.RightToLeftAutoMirrorImage = true;
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "Move first";
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.RightToLeftAutoMirrorImage = true;
            this.toolStripButton2.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton2.Text = "Move previous";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.AccessibleName = "Position";
            this.toolStripTextBox1.AutoSize = false;
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(50, 23);
            this.toolStripTextBox1.Text = "0";
            this.toolStripTextBox1.ToolTipText = "Current position";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.RightToLeftAutoMirrorImage = true;
            this.toolStripButton3.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton3.Text = "Move next";
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.RightToLeftAutoMirrorImage = true;
            this.toolStripButton4.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton4.Text = "Move last";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // householdBindingSource1BindingNavigatorSaveItem
            // 
            this.householdBindingSource1BindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.householdBindingSource1BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("householdBindingSource1BindingNavigatorSaveItem.Image")));
            this.householdBindingSource1BindingNavigatorSaveItem.Name = "householdBindingSource1BindingNavigatorSaveItem";
            this.householdBindingSource1BindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.householdBindingSource1BindingNavigatorSaveItem.Text = "Save Data";
            this.householdBindingSource1BindingNavigatorSaveItem.Click += new System.EventHandler(this.householdBindingSource1BindingNavigatorSaveItem_Click);
            // 
            // houseIDTextBox1
            // 
            this.houseIDTextBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.householdBindingSource1, "HouseID", true));
            this.houseIDTextBox1.Location = new System.Drawing.Point(91, 148);
            this.houseIDTextBox1.Name = "houseIDTextBox1";
            this.houseIDTextBox1.Size = new System.Drawing.Size(100, 20);
            this.houseIDTextBox1.TabIndex = 2;
            // 
            // addressTextBox1
            // 
            this.addressTextBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.householdBindingSource1, "Address", true));
            this.addressTextBox1.Location = new System.Drawing.Point(91, 174);
            this.addressTextBox1.Name = "addressTextBox1";
            this.addressTextBox1.Size = new System.Drawing.Size(200, 20);
            this.addressTextBox1.TabIndex = 4;
            // 
            // cityTextBox1
            // 
            this.cityTextBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.householdBindingSource1, "City", true));
            this.cityTextBox1.Location = new System.Drawing.Point(91, 200);
            this.cityTextBox1.Name = "cityTextBox1";
            this.cityTextBox1.Size = new System.Drawing.Size(100, 20);
            this.cityTextBox1.TabIndex = 6;
            // 
            // provinceTextBox1
            // 
            this.provinceTextBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.householdBindingSource1, "Province", true));
            this.provinceTextBox1.Location = new System.Drawing.Point(91, 226);
            this.provinceTextBox1.Name = "provinceTextBox1";
            this.provinceTextBox1.Size = new System.Drawing.Size(100, 20);
            this.provinceTextBox1.TabIndex = 8;
            // 
            // postalCodeTextBox1
            // 
            this.postalCodeTextBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.householdBindingSource1, "PostalCode", true));
            this.postalCodeTextBox1.Location = new System.Drawing.Point(91, 252);
            this.postalCodeTextBox1.Name = "postalCodeTextBox1";
            this.postalCodeTextBox1.Size = new System.Drawing.Size(100, 20);
            this.postalCodeTextBox1.TabIndex = 10;
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.Color.Brown;
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.btnSearch.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnSearch.Location = new System.Drawing.Point(34, 310);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(125, 30);
            this.btnSearch.TabIndex = 64;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = false;
            // 
            // btnReturn
            // 
            this.btnReturn.BackColor = System.Drawing.Color.Brown;
            this.btnReturn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.btnReturn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnReturn.Location = new System.Drawing.Point(197, 310);
            this.btnReturn.Name = "btnReturn";
            this.btnReturn.Size = new System.Drawing.Size(125, 30);
            this.btnReturn.TabIndex = 63;
            this.btnReturn.Text = "Return";
            this.btnReturn.UseVisualStyleBackColor = false;
            this.btnReturn.Click += new System.EventHandler(this.btnReturn_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 28);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(106, 89);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 66;
            this.pictureBox1.TabStop = false;
            // 
            // lblHouseHold
            // 
            this.lblHouseHold.AutoSize = true;
            this.lblHouseHold.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHouseHold.ForeColor = System.Drawing.Color.MidnightBlue;
            this.lblHouseHold.Location = new System.Drawing.Point(119, 53);
            this.lblHouseHold.Name = "lblHouseHold";
            this.lblHouseHold.Size = new System.Drawing.Size(184, 37);
            this.lblHouseHold.TabIndex = 65;
            this.lblHouseHold.Text = "HouseHold";
            // 
            // Household
            // 
            this.ClientSize = new System.Drawing.Size(359, 352);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblHouseHold);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.btnReturn);
            this.Controls.Add(postalCodeLabel);
            this.Controls.Add(this.postalCodeTextBox1);
            this.Controls.Add(provinceLabel);
            this.Controls.Add(this.provinceTextBox1);
            this.Controls.Add(cityLabel);
            this.Controls.Add(this.cityTextBox1);
            this.Controls.Add(addressLabel);
            this.Controls.Add(this.addressTextBox1);
            this.Controls.Add(houseIDLabel);
            this.Controls.Add(this.houseIDTextBox1);
            this.Controls.Add(this.householdBindingSource1BindingNavigator);
            this.Name = "Household";
            this.Load += new System.EventHandler(this.Household_Load_1);
            ((System.ComponentModel.ISupportInitialize)(this.aragonPharmacy_2DataSet2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.householdBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.householdBindingSource1BindingNavigator)).EndInit();
            this.householdBindingSource1BindingNavigator.ResumeLayout(false);
            this.householdBindingSource1BindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private AragonPharmacy_2DataSet1 aragonPharmacy_2DataSet1;
        private System.Windows.Forms.BindingSource householdBindingSource;
        private AragonPharmacy_2DataSet2TableAdapters.HouseholdTableAdapter householdTableAdapter;
        private AragonPharmacy_2DataSet1TableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator householdBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton householdBindingNavigatorSaveItem;
        private System.Windows.Forms.TextBox houseIDTextBox;
        private System.Windows.Forms.TextBox addressTextBox;
        private System.Windows.Forms.TextBox cityTextBox;
        private System.Windows.Forms.TextBox provinceTextBox;
        private System.Windows.Forms.TextBox postalCodeTextBox;
        private AragonPharmacy_2DataSet2 aragonPharmacy_2DataSet2;
        private System.Windows.Forms.BindingSource householdBindingSource1;
        private AragonPharmacy_2DataSet2TableAdapters.HouseholdTableAdapter householdTableAdapter1;
        private AragonPharmacy_2DataSet2TableAdapters.TableAdapterManager tableAdapterManager1;
        private System.Windows.Forms.BindingNavigator householdBindingSource1BindingNavigator;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripButton toolStripButton6;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton householdBindingSource1BindingNavigatorSaveItem;
        private System.Windows.Forms.TextBox houseIDTextBox1;
        private System.Windows.Forms.TextBox addressTextBox1;
        private System.Windows.Forms.TextBox cityTextBox1;
        private System.Windows.Forms.TextBox provinceTextBox1;
        private System.Windows.Forms.TextBox postalCodeTextBox1;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnReturn;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblHouseHold;
    }
}