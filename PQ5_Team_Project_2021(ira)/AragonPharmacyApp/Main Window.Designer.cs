﻿
namespace AragonPharmacyApp
{
    partial class Main_Window
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main_Window));
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCustomerEntryForm = new System.Windows.Forms.Button();
            this.btnDoctorEntryForm = new System.Windows.Forms.Button();
            this.btnDrugEntryForm = new System.Windows.Forms.Button();
            this.btnClinic = new System.Windows.Forms.Button();
            this.btnHouseHold = new System.Windows.Forms.Button();
            this.btnHealthPlan = new System.Windows.Forms.Button();
            this.btnEmployees = new System.Windows.Forms.Button();
            this.btnExitMain = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // splitter1
            // 
            this.splitter1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter1.Location = new System.Drawing.Point(0, 107);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(489, 467);
            this.splitter1.TabIndex = 0;
            this.splitter1.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(106, 89);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 25;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label1.Location = new System.Drawing.Point(124, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(288, 37);
            this.label1.TabIndex = 24;
            this.label1.Text = "Welcome Window";
            // 
            // btnCustomerEntryForm
            // 
            this.btnCustomerEntryForm.BackColor = System.Drawing.Color.Brown;
            this.btnCustomerEntryForm.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCustomerEntryForm.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnCustomerEntryForm.Location = new System.Drawing.Point(247, 133);
            this.btnCustomerEntryForm.Name = "btnCustomerEntryForm";
            this.btnCustomerEntryForm.Size = new System.Drawing.Size(192, 70);
            this.btnCustomerEntryForm.TabIndex = 26;
            this.btnCustomerEntryForm.Text = "Customer Entry Form";
            this.btnCustomerEntryForm.UseVisualStyleBackColor = false;
            this.btnCustomerEntryForm.Click += new System.EventHandler(this.btnCustomerEntryForm_Click);
            // 
            // btnDoctorEntryForm
            // 
            this.btnDoctorEntryForm.BackColor = System.Drawing.Color.Brown;
            this.btnDoctorEntryForm.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDoctorEntryForm.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnDoctorEntryForm.Location = new System.Drawing.Point(247, 316);
            this.btnDoctorEntryForm.Name = "btnDoctorEntryForm";
            this.btnDoctorEntryForm.Size = new System.Drawing.Size(192, 70);
            this.btnDoctorEntryForm.TabIndex = 27;
            this.btnDoctorEntryForm.Text = "Doctor Entry Form";
            this.btnDoctorEntryForm.UseVisualStyleBackColor = false;
            this.btnDoctorEntryForm.Click += new System.EventHandler(this.btnDoctorEntryForm_Click);
            // 
            // btnDrugEntryForm
            // 
            this.btnDrugEntryForm.BackColor = System.Drawing.Color.Brown;
            this.btnDrugEntryForm.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDrugEntryForm.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnDrugEntryForm.Location = new System.Drawing.Point(247, 227);
            this.btnDrugEntryForm.Name = "btnDrugEntryForm";
            this.btnDrugEntryForm.Size = new System.Drawing.Size(192, 70);
            this.btnDrugEntryForm.TabIndex = 28;
            this.btnDrugEntryForm.Text = "Drug Entry Form";
            this.btnDrugEntryForm.UseVisualStyleBackColor = false;
            this.btnDrugEntryForm.Click += new System.EventHandler(this.btnDrugEntryForm_Click);
            // 
            // btnClinic
            // 
            this.btnClinic.BackColor = System.Drawing.Color.Brown;
            this.btnClinic.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClinic.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnClinic.Location = new System.Drawing.Point(37, 133);
            this.btnClinic.Name = "btnClinic";
            this.btnClinic.Size = new System.Drawing.Size(192, 70);
            this.btnClinic.TabIndex = 29;
            this.btnClinic.Text = "Clinic";
            this.btnClinic.UseVisualStyleBackColor = false;
            this.btnClinic.Click += new System.EventHandler(this.btnClinic_Click);
            // 
            // btnHouseHold
            // 
            this.btnHouseHold.BackColor = System.Drawing.Color.Brown;
            this.btnHouseHold.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHouseHold.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnHouseHold.Location = new System.Drawing.Point(37, 227);
            this.btnHouseHold.Name = "btnHouseHold";
            this.btnHouseHold.Size = new System.Drawing.Size(192, 70);
            this.btnHouseHold.TabIndex = 30;
            this.btnHouseHold.Text = "House Hold";
            this.btnHouseHold.UseVisualStyleBackColor = false;
            this.btnHouseHold.Click += new System.EventHandler(this.btnHouseHold_Click);
            // 
            // btnHealthPlan
            // 
            this.btnHealthPlan.BackColor = System.Drawing.Color.Brown;
            this.btnHealthPlan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHealthPlan.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnHealthPlan.Location = new System.Drawing.Point(37, 316);
            this.btnHealthPlan.Name = "btnHealthPlan";
            this.btnHealthPlan.Size = new System.Drawing.Size(192, 70);
            this.btnHealthPlan.TabIndex = 31;
            this.btnHealthPlan.Text = "Health Plan";
            this.btnHealthPlan.UseVisualStyleBackColor = false;
            this.btnHealthPlan.Click += new System.EventHandler(this.btnHealthPlan_Click);
            // 
            // btnEmployees
            // 
            this.btnEmployees.BackColor = System.Drawing.Color.Brown;
            this.btnEmployees.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEmployees.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnEmployees.Location = new System.Drawing.Point(144, 397);
            this.btnEmployees.Name = "btnEmployees";
            this.btnEmployees.Size = new System.Drawing.Size(192, 70);
            this.btnEmployees.TabIndex = 32;
            this.btnEmployees.Text = "Employees";
            this.btnEmployees.UseVisualStyleBackColor = false;
            this.btnEmployees.Click += new System.EventHandler(this.btnEmployees_Click);
            // 
            // btnExitMain
            // 
            this.btnExitMain.BackColor = System.Drawing.Color.Black;
            this.btnExitMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExitMain.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnExitMain.Location = new System.Drawing.Point(181, 517);
            this.btnExitMain.Name = "btnExitMain";
            this.btnExitMain.Size = new System.Drawing.Size(120, 45);
            this.btnExitMain.TabIndex = 33;
            this.btnExitMain.Text = "E&xit";
            this.btnExitMain.UseVisualStyleBackColor = false;
            this.btnExitMain.Click += new System.EventHandler(this.btnExitMain_Click);
            // 
            // Main_Window
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(489, 574);
            this.Controls.Add(this.btnExitMain);
            this.Controls.Add(this.btnEmployees);
            this.Controls.Add(this.btnHealthPlan);
            this.Controls.Add(this.btnHouseHold);
            this.Controls.Add(this.btnClinic);
            this.Controls.Add(this.btnDrugEntryForm);
            this.Controls.Add(this.btnDoctorEntryForm);
            this.Controls.Add(this.btnCustomerEntryForm);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.splitter1);
            this.Name = "Main_Window";
            this.Text = "Main_Window";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCustomerEntryForm;
        private System.Windows.Forms.Button btnDoctorEntryForm;
        private System.Windows.Forms.Button btnDrugEntryForm;
        private System.Windows.Forms.Button btnClinic;
        private System.Windows.Forms.Button btnHouseHold;
        private System.Windows.Forms.Button btnHealthPlan;
        private System.Windows.Forms.Button btnEmployees;
        private System.Windows.Forms.Button btnExitMain;
    }
}