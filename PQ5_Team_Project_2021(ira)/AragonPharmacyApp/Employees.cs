﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AragonPharmacyApp
{
    public partial class Employees : Form
    {
        public Employees()
        {
            InitializeComponent();
        }

        private void employeesBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.employeesBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.aragonPharmacy_2DataSet);

        }

        private void Employees_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'aragonPharmacy_2DataSet.Employees' table. You can move, or remove it, as needed.
            this.employeesTableAdapter.Fill(this.aragonPharmacy_2DataSet.Employees);

        }

        private void btnReturnEmp_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lblEmployees_Click(object sender, EventArgs e)
        {

        }
    }
}
