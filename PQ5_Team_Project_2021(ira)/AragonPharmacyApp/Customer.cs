﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Data.Entity;
using System.Data.Entity.Validation;

namespace AragonPharmacyApp
{
    public partial class Customer : Form
    {

        public Customer()
        {
            InitializeComponent();
        }
        private AragonPharmacy_2DataSet4.AragonPharmacy_2Entities dbcontext = null;

        // private int parsedCustomerID;
        //private void RefreshContacts()
        //{
        //    //Dispose old dbContext, if any
        //    if (dbcontext != null)
        //    {
        //        dbcontext.Dispose();
        //    }
        //    //create a new dbContext so we can reorder records based on edits
        //    dbcontext = new AragonPharmacy_2DataSet4.AragonPharmacy_2Entities();

        //    // use LINQ to order the Addresses table contents
        //    // by last name, then first name
        //    dbcontext.Customers
        //        .OrderBy(entry => entry.CustLast)
        //        .ThenBy(entry => entry.CustFirst)
        //        .Load();

        //    // specify DataSource for addressBindingSource
        //    customersBindingSource.DataSource = dbcontext.Customers.Local;
        //    customersBindingSource.MoveFirst(); // go to first record

        //    custLastTextBox.Clear(); // clear the find textbox
        //}

        private void Customer_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'aragonPharmacy_2DataSet4.Customers' table. You can move, or remove it, as needed.
            this.customersTableAdapter.Fill(this.aragonPharmacy_2DataSet4.Customers);

        }


        private void btnSearch_Click(object sender, EventArgs e)
        {
        //    var lastNameQuery =
        //from customer in dbcontext.Customers
        //where customer.CustLast.StartsWith(btnSearch.Text)
        //orderby customer.CustLast, customer.CustFirst
        //select customer;

        //    display matching contacts
        //    customersBindingSource.DataSource = lastNameQuery.ToList();
        //    customersBindingSource.MoveFirst();   // go to first record

        //    don't allow add / delete when contacts are filtered 
        //    bindingNavigatorAddNewItem.Enabled = false;
        //    bindingNavigatorDeleteItem.Enabled = false;

            //    if (IsCustomerIDValid())
            //    {
            //        string connectionString = GetConnectionString();


            //        using (SqlConnection connection = new SqlConnection(connectionString))
            //        {
            //            // Define a t-SQL query string that has a parameter for orderID.
            //            const string sql = "SELECT * FROM Sales.Customer WHERE customerID = @CustID";

            //            // Create a SqlCommand object.
            //            using (SqlCommand sqlCommand = new SqlCommand(sql, connection))
            //            {
            //                // Define the @customerID parameter and set its value.
            //                sqlCommand.Parameters.Add(new SqlParameter("@CustID", SqlDbType.Int));
            //                sqlCommand.Parameters["@CustID"].Value = parsedCustomerID;

            //                try
            //                {
            //                    connection.Open();

            //                    // Run the query by calling ExecuteReader().
            //                    using (SqlDataReader dataReader = sqlCommand.ExecuteReader())
            //                    {
            //                        // Create a data table to hold the retrieved data.
            //                        DataTable dataTable = new DataTable();

            //                        // Load the data from SqlDataReader into the data table.
            //                        dataTable.Load(dataReader);

            //                        // Display the data from the data table in the data grid view.
            //                        //this.dgvCustomer.DataSource = dataTable;
            //                       // this.txtCustomerId.Text = dataTable;

            //                        // Close the SqlDataReader.
            //                        dataReader.Close();
            //                    }
            //                }
            //                catch
            //                {
            //                    MessageBox.Show("The requested customer could not be loaded.");
            //                }
            //                finally
            //                {
            //                    // Close the connection.
            //                    connection.Close();
            //                }
            //            }
            //        }
            //    }
        }

            //private bool IsCustomerIDValid()
            //{
            //   // throw new NotImplementedException();
            //    // Check for input in the customer ID text box.
            //    if (txtCustomerId.Text == "")
            //    {
            //        MessageBox.Show("Please specify the Customer ID.");
            //        return false;
            //    }

            //    // Check for characters other than integers.
            //    else if (Regex.IsMatch(txtCustomerId.Text, @"^\D*$"))
            //    {
            //        // Show message and clear input.
            //        MessageBox.Show("Customer ID must contain only numbers.");
            //        txtCustomerId.Clear();
            //        return false;
            //    }
            //    else
            //    {
            //        // Convert the text in the text box to an integer to send to the database.
            //        parsedCustomerID = Int32.Parse(txtCustomerId.Text);
            //        return true;
            //    }
            //}

            //private string GetConnectionString()
            //{
            //    // To avoid storing the connection string in your code,
            //    // you can retrieve it from a configuration file, using the
            //    // System.Configuration.ConfigurationManager.ConnectionStrings property
            //    return "Data Source=(local);Initial Catalog=AragonPharmacy_2;"
            //        + "Integrated Security=SSPI;";
            //}


  

        private void customersBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.customersBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.aragonPharmacy_2DataSet4);

        }

        private void btnReturn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dOBLabel_Click(object sender, EventArgs e)
        {

        }


        //private void btnFind_Click(object sender, EventArgs e)
        //{
        //    var lastNameQuery =
        // from customer in dbcontext.Customers
        // where customer.CustLast.StartsWith(btnSearch.Text)
        // orderby customer.CustLast, customer.CustFirst
        // select customer;

        //    // display matching contacts
        //    customersBindingSource.DataSource = lastNameQuery.ToList();
        //    customersBindingSource.MoveFirst();   // go to first record

        //    // don't allow add / delete when contacts are filtered 
        //    bindingNavigatorAddNewItem.Enabled = false;
        //    bindingNavigatorDeleteItem.Enabled = false;
        //}
    }
}
