﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AragonPharmacyApp
{
    public partial class Household : Form
    {
        public Household()
        {
            InitializeComponent();
        }

        private void householdBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.householdBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.aragonPharmacy_2DataSet1);

        }

        private void Household_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'aragonPharmacy_2DataSet1.Household' table. You can move, or remove it, as needed.
            this.householdTableAdapter.Fill(this.aragonPharmacy_2DataSet2.Household);

        }

        private void householdBindingSource1BindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.householdBindingSource1.EndEdit();
            this.tableAdapterManager1.UpdateAll(this.aragonPharmacy_2DataSet2);

        }

        private void Household_Load_1(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'aragonPharmacy_2DataSet2.Household' table. You can move, or remove it, as needed.
            this.householdTableAdapter1.Fill(this.aragonPharmacy_2DataSet2.Household);

        }

        private void btnReturn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
