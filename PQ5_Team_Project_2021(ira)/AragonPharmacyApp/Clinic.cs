﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AragonPharmacyApp
{
    public partial class Clinic : Form
    {
        public Clinic()
        {
            InitializeComponent();
        }

        private void clinicBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.clinicBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.aragonPharmacy_2DataSet1);

        }

        private void Clinic_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'aragonPharmacy_2DataSet1.Clinic' table. You can move, or remove it, as needed.
            this.clinicTableAdapter.Fill(this.aragonPharmacy_2DataSet1.Clinic);

        }

        private void clinicBindingSource1BindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.clinicBindingSource1.EndEdit();
            this.tableAdapterManager1.UpdateAll(this.aragonPharmacy_2DataSet11);

        }

        private void Clinic_Load_1(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'aragonPharmacy_2DataSet11.Clinic' table. You can move, or remove it, as needed.
            this.clinicTableAdapter1.Fill(this.aragonPharmacy_2DataSet11.Clinic);

        }

        private void btnReturnClinic_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
