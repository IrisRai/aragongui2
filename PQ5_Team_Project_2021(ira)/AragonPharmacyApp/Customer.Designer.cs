﻿
namespace AragonPharmacyApp
{
    partial class Customer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label lblCustID;
            System.Windows.Forms.Label lblCustFirst;
            System.Windows.Forms.Label lblCustLast;
            System.Windows.Forms.Label lblPhone;
            System.Windows.Forms.Label lblDOB;
            System.Windows.Forms.Label lblGender;
            System.Windows.Forms.Label lblBalance;
            System.Windows.Forms.Label lblChildCap;
            System.Windows.Forms.Label lblPlanID;
            System.Windows.Forms.Label lblHouseID;
            System.Windows.Forms.Label lblHeadHh;
            System.Windows.Forms.Label lblAllergies;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Customer));
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.btnReturn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.aragonPharmacy_2DataSet4 = new AragonPharmacyApp.AragonPharmacy_2DataSet4();
            this.customersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.customersTableAdapter = new AragonPharmacyApp.AragonPharmacy_2DataSet4TableAdapters.CustomersTableAdapter();
            this.tableAdapterManager = new AragonPharmacyApp.AragonPharmacy_2DataSet4TableAdapters.TableAdapterManager();
            this.customersBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.customersBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.txtCustID = new System.Windows.Forms.TextBox();
            this.txtCustFirst = new System.Windows.Forms.TextBox();
            this.txtCustLast = new System.Windows.Forms.TextBox();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.dtpDOB = new System.Windows.Forms.DateTimePicker();
            this.txtGender = new System.Windows.Forms.TextBox();
            this.txtBalance = new System.Windows.Forms.TextBox();
            this.cbChildCap = new System.Windows.Forms.CheckBox();
            this.txtPlanID = new System.Windows.Forms.TextBox();
            this.txtHouseID = new System.Windows.Forms.TextBox();
            this.cbHeadHh = new System.Windows.Forms.CheckBox();
            this.txtAllergies = new System.Windows.Forms.TextBox();
            this.grpBoxFindLastName = new System.Windows.Forms.GroupBox();
            this.btnFind = new System.Windows.Forms.Button();
            this.txtSearchLastName = new System.Windows.Forms.TextBox();
            this.lblSearchLastName = new System.Windows.Forms.Label();
            lblCustID = new System.Windows.Forms.Label();
            lblCustFirst = new System.Windows.Forms.Label();
            lblCustLast = new System.Windows.Forms.Label();
            lblPhone = new System.Windows.Forms.Label();
            lblDOB = new System.Windows.Forms.Label();
            lblGender = new System.Windows.Forms.Label();
            lblBalance = new System.Windows.Forms.Label();
            lblChildCap = new System.Windows.Forms.Label();
            lblPlanID = new System.Windows.Forms.Label();
            lblHouseID = new System.Windows.Forms.Label();
            lblHeadHh = new System.Windows.Forms.Label();
            lblAllergies = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aragonPharmacy_2DataSet4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.customersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.customersBindingNavigator)).BeginInit();
            this.customersBindingNavigator.SuspendLayout();
            this.grpBoxFindLastName.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblCustID
            // 
            lblCustID.AutoSize = true;
            lblCustID.Location = new System.Drawing.Point(45, 149);
            lblCustID.Name = "lblCustID";
            lblCustID.Size = new System.Drawing.Size(45, 13);
            lblCustID.TabIndex = 59;
            lblCustID.Text = "Cust ID:";
            // 
            // lblCustFirst
            // 
            lblCustFirst.AutoSize = true;
            lblCustFirst.Location = new System.Drawing.Point(45, 174);
            lblCustFirst.Name = "lblCustFirst";
            lblCustFirst.Size = new System.Drawing.Size(53, 13);
            lblCustFirst.TabIndex = 60;
            lblCustFirst.Text = "Cust First:";
            // 
            // lblCustLast
            // 
            lblCustLast.AutoSize = true;
            lblCustLast.Location = new System.Drawing.Point(45, 200);
            lblCustLast.Name = "lblCustLast";
            lblCustLast.Size = new System.Drawing.Size(54, 13);
            lblCustLast.TabIndex = 61;
            lblCustLast.Text = "Cust Last:";
            // 
            // lblPhone
            // 
            lblPhone.AutoSize = true;
            lblPhone.Location = new System.Drawing.Point(45, 226);
            lblPhone.Name = "lblPhone";
            lblPhone.Size = new System.Drawing.Size(41, 13);
            lblPhone.TabIndex = 62;
            lblPhone.Text = "Phone:";
            // 
            // lblDOB
            // 
            lblDOB.AutoSize = true;
            lblDOB.Location = new System.Drawing.Point(45, 252);
            lblDOB.Name = "lblDOB";
            lblDOB.Size = new System.Drawing.Size(33, 13);
            lblDOB.TabIndex = 63;
            lblDOB.Text = "DOB:";
            lblDOB.Click += new System.EventHandler(this.dOBLabel_Click);
            // 
            // lblGender
            // 
            lblGender.AutoSize = true;
            lblGender.Location = new System.Drawing.Point(45, 278);
            lblGender.Name = "lblGender";
            lblGender.Size = new System.Drawing.Size(45, 13);
            lblGender.TabIndex = 64;
            lblGender.Text = "Gender:";
            // 
            // lblBalance
            // 
            lblBalance.AutoSize = true;
            lblBalance.Location = new System.Drawing.Point(45, 304);
            lblBalance.Name = "lblBalance";
            lblBalance.Size = new System.Drawing.Size(49, 13);
            lblBalance.TabIndex = 65;
            lblBalance.Text = "Balance:";
            // 
            // lblChildCap
            // 
            lblChildCap.AutoSize = true;
            lblChildCap.Location = new System.Drawing.Point(336, 149);
            lblChildCap.Name = "lblChildCap";
            lblChildCap.Size = new System.Drawing.Size(55, 13);
            lblChildCap.TabIndex = 66;
            lblChildCap.Text = "Child Cap:";
            // 
            // lblPlanID
            // 
            lblPlanID.AutoSize = true;
            lblPlanID.Location = new System.Drawing.Point(336, 174);
            lblPlanID.Name = "lblPlanID";
            lblPlanID.Size = new System.Drawing.Size(45, 13);
            lblPlanID.TabIndex = 67;
            lblPlanID.Text = "Plan ID:";
            // 
            // lblHouseID
            // 
            lblHouseID.AutoSize = true;
            lblHouseID.Location = new System.Drawing.Point(336, 200);
            lblHouseID.Name = "lblHouseID";
            lblHouseID.Size = new System.Drawing.Size(55, 13);
            lblHouseID.TabIndex = 68;
            lblHouseID.Text = "House ID:";
            // 
            // lblHeadHh
            // 
            lblHeadHh.AutoSize = true;
            lblHeadHh.Location = new System.Drawing.Point(336, 226);
            lblHeadHh.Name = "lblHeadHh";
            lblHeadHh.Size = new System.Drawing.Size(55, 13);
            lblHeadHh.TabIndex = 69;
            lblHeadHh.Text = "Head HH:";
            // 
            // lblAllergies
            // 
            lblAllergies.AutoSize = true;
            lblAllergies.Location = new System.Drawing.Point(336, 255);
            lblAllergies.Name = "lblAllergies";
            lblAllergies.Size = new System.Drawing.Size(49, 13);
            lblAllergies.TabIndex = 70;
            lblAllergies.Text = "Allergies:";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(12, 28);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(106, 89);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 24;
            this.pictureBox2.TabStop = false;
            // 
            // btnReturn
            // 
            this.btnReturn.BackColor = System.Drawing.Color.Brown;
            this.btnReturn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.btnReturn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnReturn.Location = new System.Drawing.Point(292, 483);
            this.btnReturn.Name = "btnReturn";
            this.btnReturn.Size = new System.Drawing.Size(125, 30);
            this.btnReturn.TabIndex = 55;
            this.btnReturn.Text = "Return";
            this.btnReturn.UseVisualStyleBackColor = false;
            this.btnReturn.Click += new System.EventHandler(this.btnReturn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label2.Location = new System.Drawing.Point(124, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(336, 37);
            this.label2.TabIndex = 57;
            this.label2.Text = "Customer Data Form";
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.Color.Brown;
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.btnSearch.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnSearch.Location = new System.Drawing.Point(122, 483);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(125, 30);
            this.btnSearch.TabIndex = 58;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = false;
            // 
            // aragonPharmacy_2DataSet4
            // 
            this.aragonPharmacy_2DataSet4.DataSetName = "AragonPharmacy_2DataSet4";
            this.aragonPharmacy_2DataSet4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // customersBindingSource
            // 
            this.customersBindingSource.DataMember = "Customers";
            this.customersBindingSource.DataSource = this.aragonPharmacy_2DataSet4;
            // 
            // customersTableAdapter
            // 
            this.customersTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.CustomersTableAdapter = this.customersTableAdapter;
            this.tableAdapterManager.UpdateOrder = AragonPharmacyApp.AragonPharmacy_2DataSet4TableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // customersBindingNavigator
            // 
            this.customersBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.customersBindingNavigator.BindingSource = this.customersBindingSource;
            this.customersBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.customersBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.customersBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.customersBindingNavigatorSaveItem,
            this.toolStripButton1});
            this.customersBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.customersBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.customersBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.customersBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.customersBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.customersBindingNavigator.Name = "customersBindingNavigator";
            this.customersBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.customersBindingNavigator.Size = new System.Drawing.Size(550, 25);
            this.customersBindingNavigator.TabIndex = 59;
            this.customersBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // customersBindingNavigatorSaveItem
            // 
            this.customersBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.customersBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("customersBindingNavigatorSaveItem.Image")));
            this.customersBindingNavigatorSaveItem.Name = "customersBindingNavigatorSaveItem";
            this.customersBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.customersBindingNavigatorSaveItem.Text = "Save Data";
            this.customersBindingNavigatorSaveItem.Click += new System.EventHandler(this.customersBindingNavigatorSaveItem_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "toolStripButton1";
            // 
            // txtCustID
            // 
            this.txtCustID.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.customersBindingSource, "CustID", true));
            this.txtCustID.Location = new System.Drawing.Point(106, 145);
            this.txtCustID.Name = "txtCustID";
            this.txtCustID.Size = new System.Drawing.Size(46, 20);
            this.txtCustID.TabIndex = 60;
            // 
            // txtCustFirst
            // 
            this.txtCustFirst.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.customersBindingSource, "CustFirst", true));
            this.txtCustFirst.Location = new System.Drawing.Point(106, 171);
            this.txtCustFirst.Name = "txtCustFirst";
            this.txtCustFirst.Size = new System.Drawing.Size(100, 20);
            this.txtCustFirst.TabIndex = 61;
            // 
            // txtCustLast
            // 
            this.txtCustLast.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.customersBindingSource, "CustLast", true));
            this.txtCustLast.Location = new System.Drawing.Point(106, 197);
            this.txtCustLast.Name = "txtCustLast";
            this.txtCustLast.Size = new System.Drawing.Size(100, 20);
            this.txtCustLast.TabIndex = 62;
            // 
            // txtPhone
            // 
            this.txtPhone.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.customersBindingSource, "Phone", true));
            this.txtPhone.Location = new System.Drawing.Point(106, 223);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(100, 20);
            this.txtPhone.TabIndex = 63;
            // 
            // dtpDOB
            // 
            this.dtpDOB.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.customersBindingSource, "DOB", true));
            this.dtpDOB.Location = new System.Drawing.Point(106, 249);
            this.dtpDOB.Name = "dtpDOB";
            this.dtpDOB.Size = new System.Drawing.Size(200, 20);
            this.dtpDOB.TabIndex = 64;
            // 
            // txtGender
            // 
            this.txtGender.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.customersBindingSource, "Gender", true));
            this.txtGender.Location = new System.Drawing.Point(106, 275);
            this.txtGender.Name = "txtGender";
            this.txtGender.Size = new System.Drawing.Size(100, 20);
            this.txtGender.TabIndex = 65;
            // 
            // txtBalance
            // 
            this.txtBalance.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.customersBindingSource, "Balance", true));
            this.txtBalance.Location = new System.Drawing.Point(106, 301);
            this.txtBalance.Name = "txtBalance";
            this.txtBalance.Size = new System.Drawing.Size(100, 20);
            this.txtBalance.TabIndex = 66;
            // 
            // cbChildCap
            // 
            this.cbChildCap.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.customersBindingSource, "ChildCap", true));
            this.cbChildCap.Location = new System.Drawing.Point(396, 144);
            this.cbChildCap.Name = "cbChildCap";
            this.cbChildCap.Size = new System.Drawing.Size(104, 24);
            this.cbChildCap.TabIndex = 67;
            this.cbChildCap.UseVisualStyleBackColor = true;
            // 
            // txtPlanID
            // 
            this.txtPlanID.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.customersBindingSource, "PlanID", true));
            this.txtPlanID.Location = new System.Drawing.Point(396, 171);
            this.txtPlanID.Name = "txtPlanID";
            this.txtPlanID.Size = new System.Drawing.Size(100, 20);
            this.txtPlanID.TabIndex = 68;
            // 
            // txtHouseID
            // 
            this.txtHouseID.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.customersBindingSource, "HouseID", true));
            this.txtHouseID.Location = new System.Drawing.Point(396, 197);
            this.txtHouseID.Name = "txtHouseID";
            this.txtHouseID.Size = new System.Drawing.Size(100, 20);
            this.txtHouseID.TabIndex = 69;
            // 
            // cbHeadHh
            // 
            this.cbHeadHh.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.customersBindingSource, "HeadHH", true));
            this.cbHeadHh.Location = new System.Drawing.Point(397, 223);
            this.cbHeadHh.Name = "cbHeadHh";
            this.cbHeadHh.Size = new System.Drawing.Size(104, 24);
            this.cbHeadHh.TabIndex = 70;
            this.cbHeadHh.UseVisualStyleBackColor = true;
            // 
            // txtAllergies
            // 
            this.txtAllergies.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.customersBindingSource, "Allergies", true));
            this.txtAllergies.Location = new System.Drawing.Point(396, 252);
            this.txtAllergies.Name = "txtAllergies";
            this.txtAllergies.Size = new System.Drawing.Size(100, 20);
            this.txtAllergies.TabIndex = 71;
            // 
            // grpBoxFindLastName
            // 
            this.grpBoxFindLastName.Controls.Add(this.btnFind);
            this.grpBoxFindLastName.Controls.Add(this.txtSearchLastName);
            this.grpBoxFindLastName.Controls.Add(this.lblSearchLastName);
            this.grpBoxFindLastName.Location = new System.Drawing.Point(96, 371);
            this.grpBoxFindLastName.Name = "grpBoxFindLastName";
            this.grpBoxFindLastName.Size = new System.Drawing.Size(350, 70);
            this.grpBoxFindLastName.TabIndex = 72;
            this.grpBoxFindLastName.TabStop = false;
            this.grpBoxFindLastName.Text = "Find an entry by last name";
            // 
            // btnFind
            // 
            this.btnFind.Location = new System.Drawing.Point(269, 26);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(75, 20);
            this.btnFind.TabIndex = 2;
            this.btnFind.Text = "&Find";
            this.btnFind.UseVisualStyleBackColor = true;
            // 
            // txtSearchLastName
            // 
            this.txtSearchLastName.Location = new System.Drawing.Point(81, 24);
            this.txtSearchLastName.Name = "txtSearchLastName";
            this.txtSearchLastName.Size = new System.Drawing.Size(165, 20);
            this.txtSearchLastName.TabIndex = 1;
            // 
            // lblSearchLastName
            // 
            this.lblSearchLastName.AutoSize = true;
            this.lblSearchLastName.Location = new System.Drawing.Point(6, 24);
            this.lblSearchLastName.Name = "lblSearchLastName";
            this.lblSearchLastName.Size = new System.Drawing.Size(61, 13);
            this.lblSearchLastName.TabIndex = 0;
            this.lblSearchLastName.Text = "Last Name:";
            // 
            // Customer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(550, 525);
            this.Controls.Add(this.grpBoxFindLastName);
            this.Controls.Add(lblAllergies);
            this.Controls.Add(this.txtAllergies);
            this.Controls.Add(lblHeadHh);
            this.Controls.Add(this.cbHeadHh);
            this.Controls.Add(lblHouseID);
            this.Controls.Add(this.txtHouseID);
            this.Controls.Add(lblPlanID);
            this.Controls.Add(this.txtPlanID);
            this.Controls.Add(lblChildCap);
            this.Controls.Add(this.cbChildCap);
            this.Controls.Add(lblBalance);
            this.Controls.Add(this.txtBalance);
            this.Controls.Add(lblGender);
            this.Controls.Add(this.txtGender);
            this.Controls.Add(lblDOB);
            this.Controls.Add(this.dtpDOB);
            this.Controls.Add(lblPhone);
            this.Controls.Add(this.txtPhone);
            this.Controls.Add(lblCustLast);
            this.Controls.Add(this.txtCustLast);
            this.Controls.Add(lblCustFirst);
            this.Controls.Add(this.txtCustFirst);
            this.Controls.Add(lblCustID);
            this.Controls.Add(this.txtCustID);
            this.Controls.Add(this.customersBindingNavigator);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnReturn);
            this.Controls.Add(this.pictureBox2);
            this.Name = "Customer";
            this.Text = "1. Customer Data Entry Form";
            this.Load += new System.EventHandler(this.Customer_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aragonPharmacy_2DataSet4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.customersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.customersBindingNavigator)).EndInit();
            this.customersBindingNavigator.ResumeLayout(false);
            this.customersBindingNavigator.PerformLayout();
            this.grpBoxFindLastName.ResumeLayout(false);
            this.grpBoxFindLastName.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button btnReturn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSearch;
        private AragonPharmacy_2DataSet4 aragonPharmacy_2DataSet4;
        private System.Windows.Forms.BindingSource customersBindingSource;
        private AragonPharmacy_2DataSet4TableAdapters.CustomersTableAdapter customersTableAdapter;
        private AragonPharmacy_2DataSet4TableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator customersBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton customersBindingNavigatorSaveItem;
        private System.Windows.Forms.TextBox txtCustID;
        private System.Windows.Forms.TextBox txtCustFirst;
        private System.Windows.Forms.TextBox txtCustLast;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.DateTimePicker dtpDOB;
        private System.Windows.Forms.TextBox txtGender;
        private System.Windows.Forms.TextBox txtBalance;
        private System.Windows.Forms.CheckBox cbChildCap;
        private System.Windows.Forms.TextBox txtPlanID;
        private System.Windows.Forms.TextBox txtHouseID;
        private System.Windows.Forms.CheckBox cbHeadHh;
        private System.Windows.Forms.TextBox txtAllergies;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.GroupBox grpBoxFindLastName;
        private System.Windows.Forms.Button btnFind;
        private System.Windows.Forms.TextBox txtSearchLastName;
        private System.Windows.Forms.Label lblSearchLastName;
    }
}