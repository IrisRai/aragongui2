﻿
namespace AragonPharmacyApp
{
    partial class Drug
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label genericLabel;
            System.Windows.Forms.Label dINLabel;
            System.Windows.Forms.Label nameLabel;
            System.Windows.Forms.Label descriptionLabel;
            System.Windows.Forms.Label unitLabel;
            System.Windows.Forms.Label dosageLabel;
            System.Windows.Forms.Label dosageFormLabel;
            System.Windows.Forms.Label costLabel;
            System.Windows.Forms.Label priceLabel;
            System.Windows.Forms.Label feeLabel;
            System.Windows.Forms.Label interactionsLabel;
            System.Windows.Forms.Label supplierLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Drug));
            this.aragonPharmacy_2DataSet5 = new AragonPharmacyApp.AragonPharmacy_2DataSet5();
            this.drugsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.drugsTableAdapter = new AragonPharmacyApp.AragonPharmacy_2DataSet5TableAdapters.DrugsTableAdapter();
            this.tableAdapterManager = new AragonPharmacyApp.AragonPharmacy_2DataSet5TableAdapters.TableAdapterManager();
            this.drugsBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.drugsBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.genericCheckBox = new System.Windows.Forms.CheckBox();
            this.dINTextBox = new System.Windows.Forms.TextBox();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.descriptionTextBox = new System.Windows.Forms.TextBox();
            this.unitTextBox = new System.Windows.Forms.TextBox();
            this.dosageTextBox = new System.Windows.Forms.TextBox();
            this.dosageFormTextBox = new System.Windows.Forms.TextBox();
            this.costTextBox = new System.Windows.Forms.TextBox();
            this.priceTextBox = new System.Windows.Forms.TextBox();
            this.feeTextBox = new System.Windows.Forms.TextBox();
            this.interactionsTextBox = new System.Windows.Forms.TextBox();
            this.supplierTextBox = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnReturn = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblDrug = new System.Windows.Forms.Label();
            this.grpBoxFindDrugName = new System.Windows.Forms.GroupBox();
            this.btnFind = new System.Windows.Forms.Button();
            this.txtSearchDrugName = new System.Windows.Forms.TextBox();
            this.lblSearchDrugName = new System.Windows.Forms.Label();
            genericLabel = new System.Windows.Forms.Label();
            dINLabel = new System.Windows.Forms.Label();
            nameLabel = new System.Windows.Forms.Label();
            descriptionLabel = new System.Windows.Forms.Label();
            unitLabel = new System.Windows.Forms.Label();
            dosageLabel = new System.Windows.Forms.Label();
            dosageFormLabel = new System.Windows.Forms.Label();
            costLabel = new System.Windows.Forms.Label();
            priceLabel = new System.Windows.Forms.Label();
            feeLabel = new System.Windows.Forms.Label();
            interactionsLabel = new System.Windows.Forms.Label();
            supplierLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.aragonPharmacy_2DataSet5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.drugsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.drugsBindingNavigator)).BeginInit();
            this.drugsBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.grpBoxFindDrugName.SuspendLayout();
            this.SuspendLayout();
            // 
            // genericLabel
            // 
            genericLabel.AutoSize = true;
            genericLabel.Location = new System.Drawing.Point(12, 200);
            genericLabel.Name = "genericLabel";
            genericLabel.Size = new System.Drawing.Size(47, 13);
            genericLabel.TabIndex = 1;
            genericLabel.Text = "Generic:";
            // 
            // dINLabel
            // 
            dINLabel.AutoSize = true;
            dINLabel.Location = new System.Drawing.Point(12, 146);
            dINLabel.Name = "dINLabel";
            dINLabel.Size = new System.Drawing.Size(29, 13);
            dINLabel.TabIndex = 3;
            dINLabel.Text = "DIN:";
            // 
            // nameLabel
            // 
            nameLabel.AutoSize = true;
            nameLabel.Location = new System.Drawing.Point(12, 172);
            nameLabel.Name = "nameLabel";
            nameLabel.Size = new System.Drawing.Size(38, 13);
            nameLabel.TabIndex = 5;
            nameLabel.Text = "Name:";
            // 
            // descriptionLabel
            // 
            descriptionLabel.AutoSize = true;
            descriptionLabel.Location = new System.Drawing.Point(12, 226);
            descriptionLabel.Name = "descriptionLabel";
            descriptionLabel.Size = new System.Drawing.Size(63, 13);
            descriptionLabel.TabIndex = 7;
            descriptionLabel.Text = "Description:";
            // 
            // unitLabel
            // 
            unitLabel.AutoSize = true;
            unitLabel.Location = new System.Drawing.Point(12, 252);
            unitLabel.Name = "unitLabel";
            unitLabel.Size = new System.Drawing.Size(29, 13);
            unitLabel.TabIndex = 9;
            unitLabel.Text = "Unit:";
            // 
            // dosageLabel
            // 
            dosageLabel.AutoSize = true;
            dosageLabel.Location = new System.Drawing.Point(12, 278);
            dosageLabel.Name = "dosageLabel";
            dosageLabel.Size = new System.Drawing.Size(47, 13);
            dosageLabel.TabIndex = 11;
            dosageLabel.Text = "Dosage:";
            // 
            // dosageFormLabel
            // 
            dosageFormLabel.AutoSize = true;
            dosageFormLabel.Location = new System.Drawing.Point(215, 146);
            dosageFormLabel.Name = "dosageFormLabel";
            dosageFormLabel.Size = new System.Drawing.Size(73, 13);
            dosageFormLabel.TabIndex = 13;
            dosageFormLabel.Text = "Dosage Form:";
            // 
            // costLabel
            // 
            costLabel.AutoSize = true;
            costLabel.Location = new System.Drawing.Point(215, 172);
            costLabel.Name = "costLabel";
            costLabel.Size = new System.Drawing.Size(31, 13);
            costLabel.TabIndex = 15;
            costLabel.Text = "Cost:";
            // 
            // priceLabel
            // 
            priceLabel.AutoSize = true;
            priceLabel.Location = new System.Drawing.Point(215, 198);
            priceLabel.Name = "priceLabel";
            priceLabel.Size = new System.Drawing.Size(34, 13);
            priceLabel.TabIndex = 17;
            priceLabel.Text = "Price:";
            // 
            // feeLabel
            // 
            feeLabel.AutoSize = true;
            feeLabel.Location = new System.Drawing.Point(215, 224);
            feeLabel.Name = "feeLabel";
            feeLabel.Size = new System.Drawing.Size(28, 13);
            feeLabel.TabIndex = 19;
            feeLabel.Text = "Fee:";
            // 
            // interactionsLabel
            // 
            interactionsLabel.AutoSize = true;
            interactionsLabel.Location = new System.Drawing.Point(215, 250);
            interactionsLabel.Name = "interactionsLabel";
            interactionsLabel.Size = new System.Drawing.Size(65, 13);
            interactionsLabel.TabIndex = 21;
            interactionsLabel.Text = "Interactions:";
            // 
            // supplierLabel
            // 
            supplierLabel.AutoSize = true;
            supplierLabel.Location = new System.Drawing.Point(215, 276);
            supplierLabel.Name = "supplierLabel";
            supplierLabel.Size = new System.Drawing.Size(48, 13);
            supplierLabel.TabIndex = 23;
            supplierLabel.Text = "Supplier:";
            // 
            // aragonPharmacy_2DataSet5
            // 
            this.aragonPharmacy_2DataSet5.DataSetName = "AragonPharmacy_2DataSet5";
            this.aragonPharmacy_2DataSet5.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // drugsBindingSource
            // 
            this.drugsBindingSource.DataMember = "Drugs";
            this.drugsBindingSource.DataSource = this.aragonPharmacy_2DataSet5;
            // 
            // drugsTableAdapter
            // 
            this.drugsTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.DrugsTableAdapter = this.drugsTableAdapter;
            this.tableAdapterManager.UpdateOrder = AragonPharmacyApp.AragonPharmacy_2DataSet5TableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // drugsBindingNavigator
            // 
            this.drugsBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.drugsBindingNavigator.BindingSource = this.drugsBindingSource;
            this.drugsBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.drugsBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.drugsBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.drugsBindingNavigatorSaveItem});
            this.drugsBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.drugsBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.drugsBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.drugsBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.drugsBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.drugsBindingNavigator.Name = "drugsBindingNavigator";
            this.drugsBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.drugsBindingNavigator.Size = new System.Drawing.Size(419, 25);
            this.drugsBindingNavigator.TabIndex = 0;
            this.drugsBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // drugsBindingNavigatorSaveItem
            // 
            this.drugsBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.drugsBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("drugsBindingNavigatorSaveItem.Image")));
            this.drugsBindingNavigatorSaveItem.Name = "drugsBindingNavigatorSaveItem";
            this.drugsBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.drugsBindingNavigatorSaveItem.Text = "Save Data";
            this.drugsBindingNavigatorSaveItem.Click += new System.EventHandler(this.drugsBindingNavigatorSaveItem_Click);
            // 
            // genericCheckBox
            // 
            this.genericCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.drugsBindingSource, "Generic", true));
            this.genericCheckBox.Location = new System.Drawing.Point(81, 195);
            this.genericCheckBox.Name = "genericCheckBox";
            this.genericCheckBox.Size = new System.Drawing.Size(104, 24);
            this.genericCheckBox.TabIndex = 2;
            this.genericCheckBox.UseVisualStyleBackColor = true;
            // 
            // dINTextBox
            // 
            this.dINTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.drugsBindingSource, "DIN", true));
            this.dINTextBox.Location = new System.Drawing.Point(81, 143);
            this.dINTextBox.Name = "dINTextBox";
            this.dINTextBox.Size = new System.Drawing.Size(100, 20);
            this.dINTextBox.TabIndex = 4;
            // 
            // nameTextBox
            // 
            this.nameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.drugsBindingSource, "Name", true));
            this.nameTextBox.Location = new System.Drawing.Point(81, 169);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(100, 20);
            this.nameTextBox.TabIndex = 6;
            // 
            // descriptionTextBox
            // 
            this.descriptionTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.drugsBindingSource, "Description", true));
            this.descriptionTextBox.Location = new System.Drawing.Point(81, 223);
            this.descriptionTextBox.Name = "descriptionTextBox";
            this.descriptionTextBox.Size = new System.Drawing.Size(100, 20);
            this.descriptionTextBox.TabIndex = 8;
            // 
            // unitTextBox
            // 
            this.unitTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.drugsBindingSource, "Unit", true));
            this.unitTextBox.Location = new System.Drawing.Point(81, 249);
            this.unitTextBox.Name = "unitTextBox";
            this.unitTextBox.Size = new System.Drawing.Size(100, 20);
            this.unitTextBox.TabIndex = 10;
            // 
            // dosageTextBox
            // 
            this.dosageTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.drugsBindingSource, "Dosage", true));
            this.dosageTextBox.Location = new System.Drawing.Point(81, 275);
            this.dosageTextBox.Name = "dosageTextBox";
            this.dosageTextBox.Size = new System.Drawing.Size(100, 20);
            this.dosageTextBox.TabIndex = 12;
            // 
            // dosageFormTextBox
            // 
            this.dosageFormTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.drugsBindingSource, "DosageForm", true));
            this.dosageFormTextBox.Location = new System.Drawing.Point(294, 143);
            this.dosageFormTextBox.Name = "dosageFormTextBox";
            this.dosageFormTextBox.Size = new System.Drawing.Size(100, 20);
            this.dosageFormTextBox.TabIndex = 14;
            // 
            // costTextBox
            // 
            this.costTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.drugsBindingSource, "Cost", true));
            this.costTextBox.Location = new System.Drawing.Point(294, 169);
            this.costTextBox.Name = "costTextBox";
            this.costTextBox.Size = new System.Drawing.Size(100, 20);
            this.costTextBox.TabIndex = 16;
            // 
            // priceTextBox
            // 
            this.priceTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.drugsBindingSource, "Price", true));
            this.priceTextBox.Location = new System.Drawing.Point(294, 195);
            this.priceTextBox.Name = "priceTextBox";
            this.priceTextBox.Size = new System.Drawing.Size(100, 20);
            this.priceTextBox.TabIndex = 18;
            // 
            // feeTextBox
            // 
            this.feeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.drugsBindingSource, "Fee", true));
            this.feeTextBox.Location = new System.Drawing.Point(294, 221);
            this.feeTextBox.Name = "feeTextBox";
            this.feeTextBox.Size = new System.Drawing.Size(100, 20);
            this.feeTextBox.TabIndex = 20;
            // 
            // interactionsTextBox
            // 
            this.interactionsTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.drugsBindingSource, "Interactions", true));
            this.interactionsTextBox.Location = new System.Drawing.Point(294, 247);
            this.interactionsTextBox.Name = "interactionsTextBox";
            this.interactionsTextBox.Size = new System.Drawing.Size(100, 20);
            this.interactionsTextBox.TabIndex = 22;
            // 
            // supplierTextBox
            // 
            this.supplierTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.drugsBindingSource, "Supplier", true));
            this.supplierTextBox.Location = new System.Drawing.Point(294, 273);
            this.supplierTextBox.Name = "supplierTextBox";
            this.supplierTextBox.Size = new System.Drawing.Size(100, 20);
            this.supplierTextBox.TabIndex = 24;
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.Color.Brown;
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.btnSearch.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnSearch.Location = new System.Drawing.Point(56, 421);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(125, 30);
            this.btnSearch.TabIndex = 60;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = false;
            // 
            // btnReturn
            // 
            this.btnReturn.BackColor = System.Drawing.Color.Brown;
            this.btnReturn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.btnReturn.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnReturn.Location = new System.Drawing.Point(218, 421);
            this.btnReturn.Name = "btnReturn";
            this.btnReturn.Size = new System.Drawing.Size(125, 30);
            this.btnReturn.TabIndex = 59;
            this.btnReturn.Text = "Return";
            this.btnReturn.UseVisualStyleBackColor = false;
            this.btnReturn.Click += new System.EventHandler(this.btnReturn_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 28);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(106, 89);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 68;
            this.pictureBox1.TabStop = false;
            // 
            // lblDrug
            // 
            this.lblDrug.AutoSize = true;
            this.lblDrug.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDrug.ForeColor = System.Drawing.Color.MidnightBlue;
            this.lblDrug.Location = new System.Drawing.Point(124, 55);
            this.lblDrug.Name = "lblDrug";
            this.lblDrug.Size = new System.Drawing.Size(91, 37);
            this.lblDrug.TabIndex = 67;
            this.lblDrug.Text = "Drug";
            // 
            // grpBoxFindDrugName
            // 
            this.grpBoxFindDrugName.Controls.Add(this.btnFind);
            this.grpBoxFindDrugName.Controls.Add(this.txtSearchDrugName);
            this.grpBoxFindDrugName.Controls.Add(this.lblSearchDrugName);
            this.grpBoxFindDrugName.Location = new System.Drawing.Point(25, 316);
            this.grpBoxFindDrugName.Name = "grpBoxFindDrugName";
            this.grpBoxFindDrugName.Size = new System.Drawing.Size(349, 71);
            this.grpBoxFindDrugName.TabIndex = 69;
            this.grpBoxFindDrugName.TabStop = false;
            this.grpBoxFindDrugName.Text = "Find drug by name ";
            // 
            // btnFind
            // 
            this.btnFind.Location = new System.Drawing.Point(263, 28);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(75, 23);
            this.btnFind.TabIndex = 2;
            this.btnFind.Text = "&Find";
            this.btnFind.UseVisualStyleBackColor = true;
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // txtSearchDrugName
            // 
            this.txtSearchDrugName.Location = new System.Drawing.Point(78, 28);
            this.txtSearchDrugName.Name = "txtSearchDrugName";
            this.txtSearchDrugName.Size = new System.Drawing.Size(165, 20);
            this.txtSearchDrugName.TabIndex = 1;
            // 
            // lblSearchDrugName
            // 
            this.lblSearchDrugName.AutoSize = true;
            this.lblSearchDrugName.Location = new System.Drawing.Point(6, 33);
            this.lblSearchDrugName.Name = "lblSearchDrugName";
            this.lblSearchDrugName.Size = new System.Drawing.Size(64, 13);
            this.lblSearchDrugName.TabIndex = 0;
            this.lblSearchDrugName.Text = "Drug Name:";
            // 
            // Drug
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(419, 473);
            this.Controls.Add(this.grpBoxFindDrugName);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblDrug);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.btnReturn);
            this.Controls.Add(supplierLabel);
            this.Controls.Add(this.supplierTextBox);
            this.Controls.Add(interactionsLabel);
            this.Controls.Add(this.interactionsTextBox);
            this.Controls.Add(feeLabel);
            this.Controls.Add(this.feeTextBox);
            this.Controls.Add(priceLabel);
            this.Controls.Add(this.priceTextBox);
            this.Controls.Add(costLabel);
            this.Controls.Add(this.costTextBox);
            this.Controls.Add(dosageFormLabel);
            this.Controls.Add(this.dosageFormTextBox);
            this.Controls.Add(dosageLabel);
            this.Controls.Add(this.dosageTextBox);
            this.Controls.Add(unitLabel);
            this.Controls.Add(this.unitTextBox);
            this.Controls.Add(descriptionLabel);
            this.Controls.Add(this.descriptionTextBox);
            this.Controls.Add(nameLabel);
            this.Controls.Add(this.nameTextBox);
            this.Controls.Add(dINLabel);
            this.Controls.Add(this.dINTextBox);
            this.Controls.Add(genericLabel);
            this.Controls.Add(this.genericCheckBox);
            this.Controls.Add(this.drugsBindingNavigator);
            this.Name = "Drug";
            this.Text = "Drug";
            this.Load += new System.EventHandler(this.Drug_Load);
            ((System.ComponentModel.ISupportInitialize)(this.aragonPharmacy_2DataSet5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.drugsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.drugsBindingNavigator)).EndInit();
            this.drugsBindingNavigator.ResumeLayout(false);
            this.drugsBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.grpBoxFindDrugName.ResumeLayout(false);
            this.grpBoxFindDrugName.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private AragonPharmacy_2DataSet5 aragonPharmacy_2DataSet5;
        private System.Windows.Forms.BindingSource drugsBindingSource;
        private AragonPharmacy_2DataSet5TableAdapters.DrugsTableAdapter drugsTableAdapter;
        private AragonPharmacy_2DataSet5TableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator drugsBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton drugsBindingNavigatorSaveItem;
        private System.Windows.Forms.CheckBox genericCheckBox;
        private System.Windows.Forms.TextBox dINTextBox;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.TextBox descriptionTextBox;
        private System.Windows.Forms.TextBox unitTextBox;
        private System.Windows.Forms.TextBox dosageTextBox;
        private System.Windows.Forms.TextBox dosageFormTextBox;
        private System.Windows.Forms.TextBox costTextBox;
        private System.Windows.Forms.TextBox priceTextBox;
        private System.Windows.Forms.TextBox feeTextBox;
        private System.Windows.Forms.TextBox interactionsTextBox;
        private System.Windows.Forms.TextBox supplierTextBox;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnReturn;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblDrug;
        private System.Windows.Forms.GroupBox grpBoxFindDrugName;
        private System.Windows.Forms.Button btnFind;
        private System.Windows.Forms.TextBox txtSearchDrugName;
        private System.Windows.Forms.Label lblSearchDrugName;
    }
}