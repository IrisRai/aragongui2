﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AragonPharmacyApp
{
    public partial class Drug : Form
    {
        public Drug()
        {
            InitializeComponent();
        }


        private void drugsBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.drugsBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.aragonPharmacy_2DataSet5);

        }

        private void Drug_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'aragonPharmacy_2DataSet5.Drugs' table. You can move, or remove it, as needed.
            this.drugsTableAdapter.Fill(this.aragonPharmacy_2DataSet5.Drugs);

        }


        private void btnReturn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            
        }
    }
}
